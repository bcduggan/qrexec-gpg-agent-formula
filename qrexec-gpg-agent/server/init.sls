{% from "qrexec-gpg-agent/map.jinja" import qrexec_gpg_agent with context %}

{% if pillar['qubes']['type'] in ['template','standalone'] %}
packages_installed:
  pkg.installed:
    - pkgs:
      - {{ qrexec_gpg_agent.nc_pkg }}
      - {{ qrexec_gpg_agent.scdaemon_pkg }}

rpc_configured:
  file.managed:
    - name: /etc/qubes-rpc/user.GpgAgent
    - makedirs: true
    - source:
      - salt://qrexec-gpg-agent/files/qvm/server/user.GpgAgent
    - mode: 0755
{% endif %}
