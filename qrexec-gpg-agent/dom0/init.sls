{% from "qrexec-gpg-agent/map.jinja" import qrexec_gpg_agent with context %}

{% set servers = [] %}

{% for qvm,qvm_config in qrexec_gpg_agent['qvm'].items() %}
{{ qvm }}_tagged:
  qvm.tags:
    - name: {{ qvm }}
    - add:
  {% for socket,socket_config in qvm_config.items() %}
      - {{ ['qga', socket, socket_config.server, socket_config.permission]|join('_') }}
  {% endfor %}

{{ qvm }}_services_enabled:
  qvm.features:
    - name: {{ qvm }}
    - enable:
  {% for socket, socket_config in qvm_config.items() %}
    {% if socket != 'extra' %}
      {% set socket_extension = '-' + socket %}
    {% endif %}
      - service.qrexec-gpg-agent{{ socket_extension|default('') }}
  {% endfor %}

  {% for socket_config in qvm_config.values() %}
    {% do servers.append(socket_config.server) %}
  {% endfor %}
{% endfor %}

{% for socket in ['extra', 'ssh', 'browser'] %}
gpg-agent_{{ socket }}_policy_installed:
  file.managed:
    - name: /etc/qubes-rpc/policy/user.GpgAgent+{{ socket }}
    - template: jinja
    - context:
        servers: {{ servers|unique }}
        socket: {{ socket }}
    - makedirs: True
    - source:
      - salt://qrexec-gpg-agent/files/dom0/user.GpgAgent
{% endfor %}
