{% from "qrexec-gpg-agent/map.jinja" import qrexec_gpg_agent with context %}

{% if pillar['qubes']['type'] in ['template','standalone'] %}

# Disable existing gpg-agent systemd sockets and service units
  {% for socket in ['gpg-agent','gpg-agent-browser','gpg-agent-extra','gpg-agent-ssh'] %}
qrexec-gpg-agent systemd socket {{ socket }} disabled:
  file.absent:
    - name: /usr/lib/systemd/user/sockets.target.wants/{{ socket }}.socket
  {% endfor %}

# Create systemd socket and service units for exportable gpg-agent sockets
  {% for socket in ['ssh','extra','browser'] %}
    {% if socket != 'extra' %}
      {% set unit_extension = '-' + socket %}
      {% set socket_extension = '.' + socket %}
    {% endif %}
    {% set unit_extension = unit_extension|default('') %}
    {% set socket_extension = socket_extension|default('') %}
qrexec-gpg-agent{{ unit_extension }} systemd socket present:
  file.managed:
    - name: /etc/systemd/user/qrexec-gpg-agent{{ unit_extension }}.socket
    - template: jinja
    - context:
        unit_extension: '{{ unit_extension }}'
        socket_extension: '{{ socket_extension }}'
    - source:
      - salt://qrexec-gpg-agent/files/qvm/client/qrexec-gpg-agent.socket

qrexec-gpg-agent{{ unit_extension }} systemd service present:
  file.managed:
    - name: /etc/systemd/user/qrexec-gpg-agent{{ unit_extension }}@.service
    - template: jinja
    - context:
        unit_extension: '{{ unit_extension }}'
        socket: '{{ socket }}'
    - source:
      - salt://qrexec-gpg-agent/files/qvm/client/qrexec-gpg-agent@.service

qrexec-gpg-agent{{ unit_extension }} systemd socket enabled:
  file.symlink:
    - name: /etc/systemd/user/sockets.target.wants/qrexec-gpg-agent{{ unit_extension }}.socket
    - target: /etc/systemd/user/qrexec-gpg-agent{{ unit_extension }}.socket
    - makedirs: true
  {% endfor %}

  {% if qrexec_gpg_agent['ssh-agent']|default(True) %}
do_not_use_ssh-agent:
  file.comment:
    - name: /etc/X11/Xsession.options
    - regex: ^use-ssh-agent\s*$
    - char: '#'
    - backup: .orig

always_set_ssh_auth_sock:
  file.managed:
    - name: /etc/X11/Xsession.d/90ssh-auth_sock
    - source: salt://qrexec-gpg-agent/files/qvm/client/90ssh_auth_sock
    - user: root
    - group: root
    - mode: 0644
  {% endif %}

{% endif %}

{% if pillar['qubes']['type'] in ['app','standalone'] %}

  ## This works around a bug that allowed salt-ssh to collect minion AppVM
  ## grains before the AppVM set changed its hostname from its template
  ## on first boot. I could not reproduce this bug with simple example
  ## states.
  ##
  ## The pillar data should look like this to client VMs, with exactly
  ## one VM name in the qvm dictionary, the VM that this state is being
  ## applied to:
  ##
  ##   qrexec_gpg_agent:
  ##     qvm:
  ##       client_vm:
  ##         extra:
  ##           ...
  ##
  ## But before client_vm is fully booted for the first time, its
  ## hostname is its template hostname. When this bug manifests, the
  ## value of grains['id'] is still the template's hostname. Then we have
  ## no way to reference the socket dictionary with
  ## qrexec_gpg_agent['qvm']['client_vm'].
  {% set qvm_name = qrexec_gpg_agent['qvm'].keys()|first %}
  {% set sockets = qrexec_gpg_agent['qvm'][qvm_name] %}

  {% for socket,socket_config in sockets.items() %}
  {% if socket != 'extra' %}
    {% set unit_extension = '-' + socket %}
  {% endif %}
  {% set unit_extension = unit_extension|default('') %}
qrexec-gpg-agent{{ unit_extension }} gpg-agent server VM:
  file.managed:
    - name: /usr/local/lib/systemd/user/qrexec-gpg-agent{{ unit_extension }}@.service.d/00_environment.conf
    - template: jinja
    - context:
        server: '{{ socket_config.server }}'
    - makedirs: true
    - source:
      - salt://qrexec-gpg-agent/files/qvm/client/00_environment.conf
  {% endfor %}
{% endif %}
