{% if pillar['qubes']['type'] in ['app','standalone'] %}
manage_gpg_conf:
  file.managed:
    - name: /home/user/.gnupg/gpg.conf
    - user: user
    - group: user
    - mode: 0640
    - makedirs: True
    - create: True
    - allow_empty: True

disable_gpg-agent_autostart:
  file.blockreplace:
    - name: /home/user/.gnupg/gpg.conf
    - content: no-autostart
    - append_if_not_found: True
    - backup: .orig
    - append_newline: True
{% endif %}