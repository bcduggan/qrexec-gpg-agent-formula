# qrexec-gpg-agent

*qrexec-gpg-agent* allows *gpg* and *ssh* on client VMs to communicate with a *gpg-agent* process on a service VM through *qrexec*. It is an alternative to [QubesOS][]' native, excellent [Split GPG][]. It offers some advantages over Split GPG at the cost of portability (see [Portability](#Portability)).

## Usage

Invoke *gpg* and *ssh* as you would normally on client VMs. Import or create public-private key pairs only on service VMs. Import public keys only in the client VMs that require them.

## Background and motivation

### Inspired by Split SSH

The QubesOS team gifted users with Split GPG, but left Split SSH implementation as an exercise for the user. GitHub user [henn][] implemented [Split SSH][] using netcat to pipe SSH socket IO to another VM's SSH socket through qrexec.

*qrexec-gpg-agent* expands this idea to the *gpg-agent* *ssh* and *extra* sockets. The *extra* socket is designed to be exported to remote machines. Similar to Split GPG, *gpg-agent* disallows secret key modification through the *extra* socket.

*qrexec-gpg-agent* uses a socket-activated *systemd* service on client VMs to pass *gpg* IO from the client VM to a new Qubes RPC program on the service VM, */etc/qubes-rpc/user.GpgAgent*. *user.GpgAgent* starts *nc* to pass its stdio to the server's *gpg-agent* *extra* socket.

### Documented SaltStack formula for Qubes management

Deploying *qrexec-gpg-agent* requires updating *dom0*'s policies for *user.GpgAgent* and many inter-dependent modifications to TemplateVMs and AppVMs. These configurations are packaged as a SaltStack formula.

### Needs beyond Split GPG

- No GPG binary wrappers

   Split GPG requires that the user adopt Qubes-specific *gpg* wrappers. Some programs that rely on *gpg*, like *[password-store][]*, can be configured to use an alternate *gpg* binary. Those can work fine with Split GPG. But others, like a *password-store* browser plugin, *[browserpass][]*, aren't so configurable. I wanted to use *gpg* across Qubes VMs without modifying or specially configuring anything to use it.

- Context-specific public keyrings

   Split GPG also requires storing all public keys in the GPG vault VM. In addition to the security implications of allowing public key import on the vault VM, this is an inconvenient requirement. GPG keyrings are often context-specific. It would be ideal to store only secret keys on a vault VM, while client VMs only store public keys.

- SSH support

   Split GPG doesn't support using *gpg-agent* as an *ssh-agent*. QubesOS doesn't currently have the same support for SSH usage through *qrexec* that it does for GPG.

- Use GPG authentication keys as SSH keys

   If you have a GPG key with an authentication capability, you can use it as an SSH key instead of a key you generate through *ssh-keygen*.

## Installation

*qrexec-gpg-agent* is made of many moving parts that must be applied to *dom0*, the service TemplateVM, service AppVM, client TemplateVM, and client AppVM. You can quickly deploy these configurations using Qubes' SaltStack management infrastructure. But you can apply all of these configurations manually. The configurations are all described in the SaltStack states, except for adding your own GPG and SSH keys.

To deploy *qrexec-gpg-agent* with SaltStack:

1. **Copy this repository's contents to *dom0*.**

   There are a couple of methods you could use to copy the files in this repository to *dom0*. Either way, you should only transfer signed content to *dom0* and then verify that signature on *dom0* before deploying these files.

   For this section, "*vault*" is a VM without network access, "*work*" is a VM with network access. You must already have GPG signing capabilities on *vault* to self-sign the archive.

   - *Copy a self-signed tar archive:*

     1. Import your public GPG key on dom0 and verify that the fingerprint matches your key's fingerprint.

            [user@dom0 ~]$ qvm-run --pass-io vault 'gpg --armor --export me@example.com' | gpg --import
            [user@dom0 ~]$ gpg --fingerprint --list-keys me@example.com

     2. [Download an archive of this repository][] with another VM with network access and *qvm-copy* it the vault VM.

            user@work:~$ curl -O https://gitlab.com/bcduggan/qrexec-gpg-agent-formula/-/archive/master/qrexec-gpg-agent-master.tar.gz
            user@work:~$ qvm-copy qrexec-gpg-agent-master.tar.gz

     3. Inspect the contents of this archive on the vault VM. Assuming that you decide that these contents are safe, sign the archive with your GPG key on the vault VM.

            user@vault:~$ gpg --sign qrexec-gpg-agent-master.tar.gz

     5. [Copy the archive and signature to dom0][].

            [user@dom0 ~]$ qvm-run --pass-io vault 'cat qrexec-gpg-agent-master.tar.gz' > ./qrexec-gpg-agent-master.tar.gz
        	[user@dom0 ~]$ qvm-run --pass-io vault 'cat qrexec-gpg-agent-master.tar.gz.sig' > ./qrexec-gpg-agent-master.tar.gz.sig

     6. Verify the archive signature on dom0.

            [user@dom0 ~]$ gpg --verify qrexec-gpg-agent-master.tar.gz.sig

     7. Unarchive the contents to */srv/formulas/base/qrexec-gpg-agent-formula*.

            [user@dom0 ~]$ sudo tar -C /srv/formulas/base -zxf qrexec-gpg-agent-formula.tar.gz

   - *With git:*

     1. Decide that you trust that my GPG signature indicates that these files are safe.
     2. Fetch my key, import my public GPG key on dom0, and verify its fingerprint: 0x5EEBC6DAB95FF8610CBF401156A1C2EEA520ECEB.

            user@work:~$ gpg --recv-keys brian@countercontrol.tech

            [user@dom0 ~]$ qvm-run --pass-io vault 'gpg --armor --export brian@countercontrol.tech | gpg --import
            [user@dom0 ~]$ gpg --fingerprint --list-keys brian@countercontrol.tech

     3. Install git on dom0.

            [user@dom0 ~]$ sudo dnf --assumeyes install git

     4. [Clone this repository][] and its tags to the *work* VM.

            user@work:~$ git clone https://gitlab.com/bcduggan/qrexec-gpg-agent-formula.git

     5. Clone this repo and its tags to dom0:

            [root@dom0 ~]# git clone --branch v[LATEST] 'ext::qvm-run --pass-io vault-vm %S% qrexec-gpg-agent-formula' /srv/formulas/base/qrexec-gpg-agent-formula

     6. Verify the latest signed tag:

            [root@dom0 /srv/formulas/base/qrexec-gpg-agent-formula]# GNUPGHOME=/home/$SUDO_USER/.gnupg git tag --verify v[LATEST]

[Clone this repository]: https://gitlab.com/bcduggan/qrexec-gpg-agent-formula.git

2. **Add the formula directory to SaltStack configuration.**

        [root@dom0 /etc/salt]# tee master.d/formula-qrexec-gpg-agent.conf minion.d/formula-qrexec-gpg-agent.conf <<EOF
    	> file_roots:
    	>   base:
    	>     - /srv/formulas/base/qrexec-gpg-agent-formula
        > EOF

   The minion configuration applies to Salt execution targeted at *dom0*. The master configuration is copied to the management DisposableVM, where it is applied to salt execution targeted at VMs. *qrexec-gpg-agent* includes states for both *dom0* and VMs, so it must be enabled for both.

3. **Create VM SaltStack formulas.**

   The SaltStack convention in Qubes is to create formulas for each VM you want to configure, then enable the top files for the AppVMs. Enabling only the AppVMs you want is fast and convenient, but you must carefully construct all VM states and top files for this to work.

   This repository contains a *qvm.example* directory with a set of VM formulas that will build *gpg-agent* service and client VMs. Start by copying its contents to */srv/salt/qvm*. Create */srv/salt/qvm*, if it doesn't already exist:

    	[root@dom0 ~]# mkdir /usr/salt/qvm
    	[root@dom0 ~]# cp /srv/formulas/base/qrexec-gpg-agent-formula/qvm.example/* /usr/salt/qvm

4. **Enable the AppVM top files.**

   Now, enable the top files for the AppVMs you want to create and configure. In the example VM formulas, *sys-gpg-agent* is the *gpg-agent* service AppVM, and *mail* and *pass* are client AppVMs.

        [root@dom0 ~]# qubesctl top.enable qvm.sys-gpg-agent
    	[root@dom0 ~]# qubesctl top.enable qvm.mail
    	[root@dom0 ~]# qubesctl top.enable qvm.pass

5. **Create pillar data.**

   *qrexec-gpg-agent* pillar data describes which *gpg-agent* sockets (*extra*, *ssh*, and *browser*) each client VM needs and which service VMs they will try to communicate with for them. Start by copying *pillar.example* in this repository to */srv/pillar/qrexec-gpg-agent.sls*:

    	[root@dom0 ~]# cp -R /srv/formulas/base/qrexec-gpg-agent-formula/pillar.example /srv/pillar/base/qrexec-gpg-agent

   Enable the *qrexec-gpg-agent* top file:

    	[root@dom0 ~]# ln -s /srv/pillar/base/qrexec-gpg-agent/init.top /srv/pillar/_tops/base/qrexec-gpg-agent.top

6. **Apply the new states.**

   Even though you only enabled top files for three AppVMs, *sys-gpg-agent*, *mail*, and *pass*, you still need to target all of their dependencies when you apply *state.highstate*:

        [root@dom0 ~]# qubesctl --show-output --target debian-9-gpg-agent,debian-9-ws,sys-gpg-agent,mail,pass state.highstate

   The dom0 target is implied, so this command will generate RPC policies create or update all of the targeted VMs. If *mail* or *pass* already existed and did not already have the *qrexec-gpg-agent.client* state applied to them, you will need to restart them for the changes to take effect.

   The example states and top files in *qvm.example* don't apply any states to *sys-gpg-agent*. So the above *qubesctl* command will say that zero states were applied to *sys-gpg-agent* until you apply your own, like a state for importing your private keys. See *qvm.example/sys-gpg-agent.top* for details.

7. **Add keys.**

   - Import secret keys in to the service VM's keyring.
   - Import public keys in to the client VM's keyring.

8. **Test.**

   Now, test that you can see GPG private keys from a client VM:

    	user@mail:~$ *gpg* --list-secret-keys

   And test that you can see SSH private keys from a client VM:

    	user@mail:~$ ssh-add -L

   These should list the secret keys that you imported in to the service VM and display a Qubes RPC notification.

## Portability

*qrexec-gpg-agent* is not very portable. It requires:

- GnuPG 2.1+ on both the service and client VMs
- *systemd*-based Linux distributions for client VMs
- QubesOS 4.0+.

   *qrexec-gpg-agent* will probably work on QubesOS 3.2. But 4.0+ has been tested and supports VM tags, which can make writing policies much easier.

## TODO

- [ ] Explore using *gpg-agent* logs to determine which host executed which action.
  - [x] It was a longshot, but verified that the *gpg-agent* logs don't include host information.
  - [ ] Explore using *gpg-agent* logs to identify what operation the *gpg* application is attempting to perform.
- [ ] Support Fedora VMs.
- [ ] Support non-systemd-based operating systems, possibly even [Windows 10][]
- [ ] Support custom VM users (not *user*)
- [ ] Support SaltStack use through management VMs
  - [ ] Depends on the ability to edit RPC policies from a management VM, which hasn't been released, yet.

[QubesOS]: https://www.qubes-os.org/
[Split GPG]: https://www.qubes-os.org/doc/split-gpg/
[password-store]: https://www.passwordstore.org/
[browserpass]: https://github.com/browserpass/browserpass
[henn]: https://github.com/henn
[QubesOS Salt]: https://www.qubes-os.org/doc/salt/
[gpg-agent extra socket]: https://wiki.gnupg.org/AgentForwarding
[QubesOS qrexec3]: https://www.qubes-os.org/doc/qrexec3/
[Split SSH]: https://github.com/henn/qubes-app-split-ssh
[Download an archive of this repository]: https://gitlab.com/bcduggan/qrexec-gpg-agent-formula/-/archive/master/qrexec-gpg-agent-master.tar.gz
[Copy the archive and signature to dom0]: https://www.qubes-os.org/doc/copy-from-dom0/#copying-to-dom0
[Windows 10]: https://blogs.msdn.microsoft.com/commandline/2017/12/19/af_unix-comes-to-windows/
