debian-9-gpg-agent_cloned:
  qvm.vm:
    - name: debian-9-gpg-agent
    - clone:
      - source: debian-9
      - label: black

debian-9-gpg-agent_labeled:
  qvm.prefs:
    - name: debian-9-gpg-agent
    - label: black
