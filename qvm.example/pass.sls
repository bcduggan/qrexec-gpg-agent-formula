include:
  - qvm.debian-9-ws

pass_present:
  qvm.present:
    - name: pass
    - template: debian-9-ws
    - netvm: sys-net
    - label: blue
    - require:
        - sls: qvm.debian-9-ws
    - require_in:
        - sls: qrexec-gpg-agent.dom0
