include:
  - qvm.debian-9-ws

mail_present:
  qvm.present:
    - name: mail
    - template: debian-9-ws
    - netvm: sys-net
    - label: blue
    - require:
        - sls: qvm.debian-9-ws
    - require_in:
        - sls: qrexec-gpg-agent.dom0
