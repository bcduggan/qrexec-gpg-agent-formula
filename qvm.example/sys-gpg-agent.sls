include:
  - qvm.debian-9-gpg-agent

sys-gpg-agent_present:
  qvm.present:
    - name: sys-gpg-agent
    - template: debian-9-gpg-agent
    - label: green
    - require:
      - sls: qvm.debian-9-gpg-agent

sys-gpg-agent_prefs:
  qvm.prefs:
    - name: sys-gpg-agent
    - template: debian-9-gpg-agent
    - label: green
    - netvm: ''
    - autostart: True
    - require:
      - sys-gpg-agent_present
