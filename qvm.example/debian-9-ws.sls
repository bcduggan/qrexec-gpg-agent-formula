debian-9_workstation:
  qvm.vm:
    - name: debian-9-ws
    - clone:
      - source: debian-9
      - label: black

debian-9-ws_labeled:
  qvm.prefs:
    - name: debian-9-ws
    - label: black
