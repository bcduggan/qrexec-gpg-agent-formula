qrexec_gpg_agent:
  qvm:
    pass:
      extra:
        server: sys-gpg-agent
        permission: allow
      ssh:
        server: sys-gpg-agent
        permission: allow
