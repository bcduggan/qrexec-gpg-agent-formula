qrexec_gpg_agent:
  qvm:
    mail:
      extra:
        server: sys-gpg-agent
        permission: ask
      ssh:
        server: sys-gpg-agent
        permission: ask
